# Curv'it

![Curv'it](https://forge.apps.education.fr/romain.casati/curvit/-/raw/master/src/logo/logo-grey-grid.svg)

## description

[curvit.fr](https://curvit.fr/) is a web app to design real functions using piecewise cubic Bézier curves.
It supports derivative plot and non uniq image detection and can export in
many formats (SVG, PNG, JPG, PDF and LaTeX using pgfplots).

## micro user manual

Use left-click on the curve to add a control point and right-click to remove it.

Moving a tangent handle while pressing `ctrl` key will force tangent direction continuity.
If you press `ctrl + maj`, you disable the tangent continuity.

## the curv'it logo

The [curv'it](https://curvit.fr) logo was designed under [curv'it](https://curvit.fr)!

## contact

Contact the author at `romain . casati @ basthon.fr`.
