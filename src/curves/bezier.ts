// helper functions
const sqr = (x: number) => x * x;
const dist2 = (p1: number[], p2: number[]) =>
  p1.map((_, i) => sqr(p2[i] - p1[i])).reduce((a, b) => a + b, 0);
const dist = (p1: number[], p2: number[]) => Math.sqrt(dist2(p1, p2));

type Point = [number, number];
type Bezier = Point[];

// operate component-wise
function op<Type>(f: (...args: Type[]) => Type, ...args: Type[][]): Type[] {
  return args[0].map((_, i) => f(...args.map((v) => v[i])));
}

// operate component-wise and affect to target
function opTo<Type>(
  f: (...args: Type[]) => Type,
  target: Type[],
  ...args: Type[][]
): void {
  target.forEach((_, i) => (target[i] = f(...args.map((v) => v[i]))));
}

// sum function component-wise
function sum(f: (...args: number[]) => number, ...args: number[][]): number {
  return args[0].reduce((a, _, i) => a + f(...args.map((v) => v[i])), 0);
}

const barycentre = (t: number, p1: Point, p2: Point): Point =>
  op((x, y) => (1 - t) * x + t * y, p1, p2) as Point;

const deCasteljau = (
  curve: Bezier,
  t: number,
  niter: number = 1,
  keep: boolean = false
): Bezier => {
  let ctl = curve;
  const resHead: Bezier = [];
  const resTail: Bezier = [];
  for (let k = 0; k < niter; k++) {
    if (keep) {
      resHead.push(ctl[0]);
      resTail.unshift(...ctl.slice(-1));
    }
    ctl = ctl.reduce((a, _, i) => {
      if (i > 0) a.push(barycentre(t, ctl[i - 1], ctl[i]));
      return a;
    }, [] as Bezier);
  }
  return [...resHead, ...ctl, ...resTail];
};

const pointAt = (curve: Bezier, t: number): Point => {
  return deCasteljau(curve, t, curve.length - 1)[0];
};

const derivativeCurve = (curve: Bezier): Bezier => {
  const n = curve.length - 1;
  return curve.reduce((a, _, k) => {
    if (k > 0)
      a.push(op((x, y) => n * (y - x), curve[k - 1], curve[k]) as Point);
    return a;
  }, [] as Bezier);
};

const closestPointOnCurve = (curve: Bezier, point: Point): number => {
  const diff = derivativeCurve(curve);
  const diff2 = derivativeCurve(diff);
  const f = (t: number) => dist2(pointAt(curve, t), point) / 2;
  const fdiff = (t: number) => {
    const p = pointAt(curve, t);
    const pd = pointAt(diff, t);
    return sum((x, y, p) => y * (x - p), p, pd, point);
  };
  const fdiff2 = (t: number) => {
    const p = pointAt(curve, t);
    const pd = pointAt(diff, t);
    const pd2 = pointAt(diff2, t);
    return sum((x, y, z, p) => z * (x - p) + y * y, p, pd, pd2, point);
  };
  // Newton-Raphson on fdiff with t0 = 0.5
  let t = 0.5;
  let niter = 0;
  while (niter < 5) {
    const fd = fdiff(t);
    t = Math.max(0, Math.min(t - fd / fdiff2(t), 1));
    niter += 1;
    if (Math.abs(fd) < 1e-5) break;
  }
  return t;
};

/**
 * Does the cubic curve actually represents the graph of a scalar function
 * (with a uniq image for any x)
 * We test the positivity of x'(t) wich is an order 2
 */
const uniqImage = (curve: Bezier): boolean => {
  if (
    Math.pow(curve[2][0] - curve[1][0], 2) -
      (curve[3][0] - curve[2][0]) * (curve[1][0] - curve[0][0]) <=
    0
  )
    return true;
  else {
    const a = curve[3][0] - 3 * curve[2][0] + 3 * curve[1][0] - curve[0][0];
    const b = 2 * (curve[0][0] + curve[2][0] - 2 * curve[1][0]);
    const c = curve[1][0] - curve[0][0];
    if (a === 0) return b === 0 || !(0 < -c / b && -c / b < 1);
    const delta = b * b - 4 * a * c;
    const x1 = (-b - Math.sqrt(delta)) / (2 * a);
    const x2 = (-b + Math.sqrt(delta)) / (2 * a);
    return !((0 < x1 && x1 < 1) || (0 < x2 && x2 < 1));
  }
};

export {
  type Bezier,
  type Point,
  pointAt,
  deCasteljau,
  derivativeCurve,
  closestPointOnCurve,
  uniqImage,
  sqr,
  dist,
  dist2,
  op,
  opTo,
  sum,
};
