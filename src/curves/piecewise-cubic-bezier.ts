import {
  Bezier,
  Point,
  pointAt as pointAtBezier,
  deCasteljau,
  closestPointOnCurve as closestPointOnBezier,
  derivativeCurve,
  uniqImage as uniqImageCubic,
  sqr,
  dist,
  dist2,
  opTo,
  op,
  sum,
} from "./bezier";

type PiecewiseCubicBezier = Point[][];

const cubicCurve = (curve: PiecewiseCubicBezier, index: number): Bezier => {
  const ctl1 = curve[index];
  const ctl2 = curve[index + 1];
  return [...ctl1.slice(-2), ...ctl2.slice(0, 2)];
};

const pointsAt = (curve: PiecewiseCubicBezier, ts: number[]): Point[] => {
  const n = curve.length - 1;
  const cubics = curve.reduce((a, _, k) => {
    if (k > 0) a.push(cubicCurve(curve, k - 1) as Bezier);
    return a;
  }, [] as Bezier[]);
  return ts.map((t) => {
    const k = Math.min(Math.floor(t * n), n - 1);
    return pointAtBezier(cubics[k], n * t - k);
  });
};

const pointAt = (
  curve: PiecewiseCubicBezier,
  index: number,
  t: number
): Point => {
  return pointAtBezier(cubicCurve(curve, index), t);
};

const moveControlPoint = (
  curve: PiecewiseCubicBezier,
  cubicIndex: number,
  pointIndex: number,
  delta: Point,
  continuity: number
): void => {
  const group = curve[cubicIndex];
  if ([cubicIndex === 0, cubicIndex > 0, false][pointIndex]) {
    // move a point on the curve
    group.forEach((p) => opTo((x, y) => x + y, p, p, delta));
  } else {
    const p0 = group[pointIndex];
    // start moving other point
    if (group.length > 2) {
      const p1 = group[1];
      const p2 = group[2 - pointIndex];
      const n1 = dist(p1, p2);
      if (continuity === 1) {
        const u = op((x1, x2) => (x2 - x1) / n1, p1, p2);
        const du = sum((x, y) => x * y, delta, u);
        opTo((x) => du * x, delta, u);
      } else if (continuity > 1) {
        const n0 = dist(p0, p1);
        if (n0 !== 0)
          opTo(
            (x0, x1, d) => x1 - (n1 / n0) * (x0 + d - x1),
            p2,
            p0,
            p1,
            delta
          );
      }
    }
    // effectively move our point
    opTo((x, y) => x + y, p0, p0, delta);
  }
};

const derivativesAt = (curve: PiecewiseCubicBezier, ts: number[]): Point[] => {
  const n = curve.length - 1;
  const derivarives = curve.reduce((a, _, k) => {
    if (k > 0) a.push(derivativeCurve(cubicCurve(curve, k - 1)) as Bezier);
    return a;
  }, [] as Bezier[]);
  return ts.map((t) => {
    const k = Math.min(Math.floor(t * n), n - 1);
    return pointAtBezier(derivarives[k], n * t - k);
  });
};

const scalarDerivativesAt = (
  curve: PiecewiseCubicBezier,
  ts: number[],
  smooth: boolean = false
): Point[] | PiecewiseCubicBezier => {
  const points = pointsAt(curve, ts);
  const derivs = derivativesAt(curve, ts).map((v) => v[1] / v[0]);
  const sampling: Point[] = points.map((v, i) => [v[0], derivs[i]]);
  if (smooth) return smoothSampling(sampling);
  else return sampling;
};

const smoothSampling = (
  sampling: Point[],
  smoothingFactor: number = 0.2
): PiecewiseCubicBezier => {
  const res: PiecewiseCubicBezier = [];
  for (let k = 0; k < sampling.length; k++) {
    const p1 = sampling[k];
    const p0 = k === 0 ? p1 : sampling[k - 1];
    const p2 = k + 1 === sampling.length ? p1 : sampling[k + 1];
    const line = op((x, y) => y - x, p0, p2) as Point;
    const c0 = op((x, y) => x - y * smoothingFactor, p1, line) as Point;
    const c2 = op((x, y) => x + y * smoothingFactor, p1, line) as Point;
    const ctl = [p1];
    if (k > 0) ctl.splice(0, 0, c0);
    if (k + 1 < sampling.length) ctl.push(c2);
    res.push(ctl);
  }
  return res;
};

const piecewiseScalarDerivative = (
  curve: PiecewiseCubicBezier,
  pointsPerBezier: number,
  smooth: boolean = false
): Point[][] | PiecewiseCubicBezier[] => {
  const npts = pointsPerBezier;
  const ts = Array.from({ length: npts }, (_, i) => i / (npts - 1));
  const sampling = curve.reduce((a, _, k) => {
    if (k > 0) {
      const cubic = cubicCurve(curve, k - 1);
      const derivative = derivativeCurve(cubic);
      a.push(
        ts.map((t) => {
          const p = pointAtBezier(cubic, t);
          const d = pointAtBezier(derivative, t);
          return [p[0], d[1] / d[0]];
        })
      );
    }
    return a;
  }, [] as Point[][]);
  if (smooth) return sampling.map((s) => smoothSampling(s));
  else return sampling;
};

const addPointAt = (
  curve: PiecewiseCubicBezier,
  index: number,
  t: number
): void => {
  const cubic = cubicCurve(curve, index);
  const subdiv = deCasteljau(cubic, t, 3, true);
  curve[index].splice(-1, 1, subdiv[1]);
  curve[index + 1][0] = subdiv.slice(-2)[0];
  curve.splice(index + 1, 0, subdiv.slice(2, 5));
};

const closestPointOnCurve = (
  curve: PiecewiseCubicBezier,
  point: Point
): [number, number] => {
  let tMin: number = 0;
  let indexMin: number = 0;
  let dMin: number = Infinity;
  for (let k = 0; k < curve.length - 1; k++) {
    const cubic = cubicCurve(curve, k);
    const t = closestPointOnBezier(cubic, point);
    const d = dist2(pointAt(curve, k, t), point);
    if (d < dMin) [dMin, tMin, indexMin] = [d, t, k];
  }
  return [indexMin, tMin];
};

const addPointHere = (curve: PiecewiseCubicBezier, point: Point): void => {
  const [index, t] = closestPointOnCurve(curve, point);
  addPointAt(curve, index, t);
};

const removePoint = (curve: PiecewiseCubicBezier, cubicIndex: number): void => {
  // do not remove when there is only one Bezier curve
  if (curve.length < 3) return;
  // specific case of first and last points
  if (cubicIndex === 0) curve[1].splice(0, 1);
  else if (cubicIndex === curve.length - 1)
    curve[curve.length - 2].splice(-1, 1);
  // default case
  curve.splice(cubicIndex, 1);
};

const uniqImage = (curve: PiecewiseCubicBezier): boolean => {
  // per cubic test
  const uniqEach = curve.reduce(
    (a, _, k) => (k > 0 ? a && uniqImageCubic(cubicCurve(curve, k - 1)) : a),
    true
  );
  // case of a curve with vertical tangent that comes backward
  // we verify that interpolating control points are in monotonous
  // order in abscissa
  const dxs: number[] = curve
    .map((v, i) => v[i === 0 ? 0 : 1][0])
    .reduce((a, _, k, arr) => {
      if (k > 0) a.push(arr[k] - arr[k - 1]);
      return a;
    }, [] as number[]);
  const monotonous = dxs.reduce(
    (a, _, k) => (k > 0 ? a && dxs[k - 1] * dxs[k] >= 0 : a),
    true
  );
  return uniqEach && monotonous;
};

export {
  type PiecewiseCubicBezier,
  type Point,
  pointAt,
  moveControlPoint,
  addPointHere,
  removePoint,
  scalarDerivativesAt,
  piecewiseScalarDerivative,
  uniqImage,
  sqr,
  dist2,
  dist,
};
