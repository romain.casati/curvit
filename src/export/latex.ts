import { stateManager, State } from "../state/state-manager";
import {
  piecewiseScalarDerivative as derivative,
  PiecewiseCubicBezier,
} from "../curves/piecewise-cubic-bezier";

const pointToStr = (p: number[]): string => `(axis cs: ${p.join(", ")})`;

const curveToDraw = (curve: [number, number][][]): string => {
  const points = curve.map((ctl) => ctl.map(pointToStr)).flat();
  const tokens = [" .. controls ", " and ", " .. "];
  const pointAndToken = (p: string, k: number) =>
    p + (k < points.length - 1 ? tokens[k % tokens.length] : "");
  return points.map(pointAndToken).join("");
};

const curveToDrawHandles = (curve: [number, number][][]): string => {
  // FIXME: all this circle (2pt) are ugly... find a smart way to
  // draw marks on each points of a path like in plot coordinates
  // with option mark=*
  return curve
    .map((ctl) => ctl.map((p) => pointToStr(p) + " circle (2pt)").join(" -- "))
    .join(" ");
};

const curveToPgfplots = (state: State) => {
  const xlim = stateManager.xlim(state);
  const ylim = stateManager.ylim(state);
  const minorUnits = stateManager.minorUnits(state);
  const curve = stateManager.curve(state);
  let handles = "";
  if (stateManager.showHandles(state))
    handles = `\
  \\definecolor{handlescolor}{HTML}{d32f2f}
  \\draw[handlescolor, fill] ${curveToDrawHandles(curve)};
`;
  let derivatives = "";
  if (stateManager.showDerivative(state)) {
    const pcb = derivative(curve, 15, true) as PiecewiseCubicBezier[];
    derivatives = `\
  \\definecolor{derivativecolor}{HTML}{2e7d32}
  \\begin{scope}[derivativecolor]
${pcb.map((p) => "    \\draw " + curveToDraw(p) + ";").join("\n")}
  \\end{scope}
`;
  }

  return `\
\\begin{axis}[
  axis equal image,
  axis lines = center,
  width = 15cm,
${stateManager.showGrid(state) ? "  grid = both,\n" : ""}\
  xtick distance = 1,
  ytick distance = 1,
  minor tick num = ${minorUnits - 1},
  xmin = ${xlim[0]},
  xmax = ${xlim[1]},
  ymin = ${ylim[0]},
  ymax = ${ylim[1]}]
  %
  \\definecolor{curvecolor}{HTML}{1976d2}
  \\draw[curvecolor] ${curveToDraw(curve)};
${handles}\
${derivatives}\
  %
\\end{axis}`;
};

const curveToLatex = (state: State) => {
  return `\
\\documentclass[11pt]{standalone}

\\usepackage[utf8]{inputenc}
\\usepackage{textcomp}
\\usepackage[T1]{fontenc}

\\usepackage{pgfplots}
\\pgfplotsset{compat=1.18}
\\tikzset{
  every picture/.style={thick, line cap=round}
}
\\pgfplotsset{
	every tick/.style={black, thick},
	grid style={thin, gray},
	/pgf/number format/use comma,
	/pgf/number format/1000 sep={\ }
}
\\begin{document}
\\begin{tikzpicture}
${curveToPgfplots(state)}
\\end{tikzpicture}
\\end{document}`;
};

export { curveToPgfplots, curveToLatex };
