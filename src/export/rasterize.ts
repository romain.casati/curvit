const svgToImg = async (svg: string): Promise<HTMLImageElement> => {
  const img = new Image();
  img.src = `data:image/svg+xml;charset=utf-8,${encodeURIComponent(svg)}`;
  await new Promise((resolve, reject) => {
    img.onload = resolve;
    img.onerror = reject;
  });
  return img;
};

const imgToCanvas = async (
  img: HTMLImageElement,
  definition: number,
  whiteBackground: boolean
): Promise<HTMLCanvasElement> => {
  const canvas = document.createElement("canvas");
  canvas.width = Math.round(Math.sqrt((definition * img.width) / img.height));
  canvas.height = Math.round(Math.sqrt((definition * img.height) / img.width));
  const context = canvas.getContext("2d");
  if (whiteBackground) {
    context!.fillStyle = "#fff";
    context?.fillRect(0, 0, canvas.width, canvas.height);
  }
  context?.drawImage(img, 0, 0, canvas.width, canvas.height);
  return canvas;
};

const rasterizeSVG = async (
  svg: string,
  format: "png" | "jpeg",
  definition: number,
  quality: number
): Promise<Blob | null> => {
  const img = await svgToImg(svg);
  const canvas = await imgToCanvas(img, definition, format === "jpeg");
  return await new Promise<Blob | null>((resolve) =>
    canvas.toBlob(resolve, `image/${format}`, quality)
  );
};

export { rasterizeSVG };
