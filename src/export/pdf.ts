const stringToNode = (html: string): Node | null => {
  const parser = new DOMParser();
  const element = parser.parseFromString(html, "text/html");
  return element.body.firstChild;
};

const svgToPdfBlob = async (svg: string): Promise<Blob> => {
  // dynamic import to allow webpack splitting things
  const jsPDF = (await import("jspdf")).jsPDF;
  await import("svg2pdf.js");

  const element = stringToNode(svg) as SVGElement;
  const width = +element.getAttribute("width")!;
  const height = +element.getAttribute("height")!;

  // new pdf
  const doc = new jsPDF(width > height ? "l" : "p", "px", [width, height]);
  // add svg
  await doc.svg(element, { width, height });
  // export
  return new Blob([doc.output()], { type: "application/pdf" });
};

export { svgToPdfBlob };
