import { stateManager, State } from "../state/state-manager";
import { curveToPgfplots, curveToLatex } from "./latex";
import { rasterizeSVG } from "./rasterize";
import { svgToPdfBlob } from "./pdf";

const exportFormats = [
  "svg",
  "png",
  "jpeg",
  "pdf",
  "latex",
  "pgfplots",
  "curvit",
] as const;
type ExportFormat = (typeof exportFormats)[number];

const formatToLabel = (format: ExportFormat): string => {
  switch (format) {
    case "jpeg":
      return "jpg";
    case "latex":
      return "LaTeX";
    default:
      return format;
  }
};

const curveToBlob = async (state: State, svg: string): Promise<Blob | null> => {
  const format = stateManager.exportFormat(state);
  switch (format) {
    case "svg":
      return new Blob([svg], {
        type: "image/svg+xml;charset=utf-8",
      });
    case "pdf":
      return await svgToPdfBlob(svg);
    case "png":
    case "jpeg":
      return await rasterizeSVG(svg, format, Math.pow(2, 23), 0.95);
    case "latex":
      return new Blob([curveToLatex(state)], {
        type: "text/plain;charset=utf-8",
      });
    case "pgfplots":
      return new Blob([curveToPgfplots(state)], {
        type: "text/plain;charset=utf-8",
      });
    case "curvit":
      return new Blob([JSON.stringify(stateManager.filter(state))], {
        type: "text/plain;charset=utf-8",
      });
    default:
      throw new Error("internal error, format not recognized");
  }
};

const formatToExt = (format: ExportFormat): string => {
  switch (format) {
    case "jpeg":
      return "jpg";
    case "latex":
    case "pgfplots":
      return "tex";
    default:
      return format;
  }
};

const exportAndDownloadCurve = async (
  state: State,
  svg: string,
  filename: string
): Promise<void> => {
  const format = stateManager.exportFormat(state);
  const blob = await curveToBlob(state, svg);
  // dynamic import to allow webpack splitting things
  const saveAs = (await import("file-saver")).default;
  if (blob != null) saveAs(blob, `${filename}.${formatToExt(format)}`);
};

export {
  exportFormats,
  type ExportFormat,
  curveToBlob,
  exportAndDownloadCurve,
  formatToLabel,
};
