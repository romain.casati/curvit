import React from "react";
import ReactDOM from "react-dom/client";
import "./components/i18n";
import App from "./components/app";

const root = document.createElement("div");
document.body.appendChild(root);

ReactDOM.createRoot(root).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
