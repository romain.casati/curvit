import { MutableRefObject, useRef, useMemo, useEffect } from "react";
import { useTranslation } from "react-i18next";

import {
  SVG,
  Container,
  PointArray,
  Path,
  Polyline,
  Circle,
} from "@svgdotjs/svg.js";
import "@svgdotjs/svg.draggable.js";
import {
  PiecewiseCubicBezier,
  Point,
  moveControlPoint,
  removePoint,
  addPointHere,
  piecewiseScalarDerivative as derivative,
  uniqImage,
  sqr,
  dist2,
} from "../curves/piecewise-cubic-bezier";

interface CanvasProps extends React.HTMLAttributes<HTMLDivElement> {
  xlim: [number, number];
  ylim: [number, number];
  minorUnits: number;
  showAxis: boolean;
  showGrid: boolean;
  showHandles: boolean;
  showDerivative: boolean;
  improveTouch: boolean;
  continuity: number;
  curve: PiecewiseCubicBezier;
  onSetCurve: (_: PiecewiseCubicBezier) => void;
  svgRef: MutableRefObject<Container | null>;
  uniqImageRef: MutableRefObject<HTMLButtonElement | null>;
}

const addGrid = (
  grid: Container,
  xlim: [number, number],
  ylim: [number, number],
  minorUnits: number
) => {
  const [xmin, xmax] = xlim;
  const [ymin, ymax] = ylim;

  // color
  grid.stroke("#969696");
  // set grid stroke width to half parent stroke width
  const parentStrokeWidth = grid.parent()?.attr("stroke-width");
  grid.attr({ "stroke-width": parentStrokeWidth / 2 });
  // vertical lines
  for (let i = 0; i <= (xmax - xmin) * minorUnits; i++)
    grid.line(xmin + i / minorUnits, ymin, xmin + i / minorUnits, ymax);
  // horizontal lines
  for (let j = 0; j <= (ymax - ymin) * minorUnits; j++)
    grid.line(xmin, ymin + j / minorUnits, xmax, ymin + j / minorUnits);
};

const addAxis = (
  axis: Container,
  xlim: [number, number],
  ylim: [number, number]
) => {
  const [xmin, xmax] = xlim;
  const [ymin, ymax] = ylim;

  // font size
  const fontSize = axis.parent()?.attr("font-size");
  // x axis
  axis.line(xmin, 0, xmax, 0);
  // y axis
  axis.line(0, ymin, 0, ymax);
  // xlabels
  let g = axis.group().attr({
    transform: "matrix(1, 0, 0, -1, 0, 0)",
    "stroke-width": 0,
    fill: "currentcolor",
  });
  for (let x = xmin + 1; x < xmax; x++) {
    if (x === 0) continue;
    const txt = g.plain(x.toString());
    txt.move(x - txt.length() / 2, 0);
    axis.line(x, 0, x, 0.05);
  }
  // ylabels
  g = axis.group().attr({
    transform: "matrix(1, 0, 0, -1, 0, 0)",
    "stroke-width": 0,
    fill: "currentcolor",
  });
  for (let y = ymin + 1; y < ymax; y++) {
    if (y === 0) continue;
    const txt = g.plain(y.toString());
    txt.move(-txt.length() - 0.4 * fontSize, -y - 0.8 * fontSize);
    axis.line(0, y, 0.05, y);
  }
  // 0
  const txt = g.plain("0");
  txt.move(0 - txt.length() - 0.4 * fontSize, 0);
};

const curveToString = (curve: PiecewiseCubicBezier) => {
  const ctl = curve;
  return ctl
    .reduce(
      (a, _, k) => {
        if (k > 0)
          a.push(
            "C",
            [...ctl[k - 1].slice(-1), ...ctl[k].slice(0, 2)]
              .map((v) => v.join(" "))
              .join(",")
          );
        return a;
      },
      ["M", ctl[0][0].join(",")]
    )
    .join(" ");
};

const addCurve = (
  group: Container,
  curve: PiecewiseCubicBezier,
  onSetCurve: (_: PiecewiseCubicBezier) => void
): Path => {
  const parentStrokeWidth = group.parent()?.attr("stroke-width");
  group.attr({ "stroke-width": (parentStrokeWidth / 3) * 5 });

  return group
    .path(curveToString(curve))
    .attr({
      stroke: "#1976d2",
      "stroke-opacity": 1,
      cursor: "crosshair",
    })
    .on("click", (e: Event) => {
      e.preventDefault();
      const _e = e as MouseEvent;
      const p = group.point(_e.pageX, _e.pageY);
      addPointHere(curve, [p.x, p.y]);
      onSetCurve(curve);
    });
};

const addDerivative = (
  group: Container,
  curve: PiecewiseCubicBezier,
  npts: number
): Path[] => {
  const parentStrokeWidth = group.parent()?.attr("stroke-width");
  group.attr({ "stroke-width": (parentStrokeWidth / 3) * 5 });
  const pcb = derivative(curve, npts, true) as PiecewiseCubicBezier[];
  return pcb.map((p) =>
    group.path(curveToString(p)).attr({
      stroke: "#2e7d32",
      "stroke-opacity": 1,
    })
  );
};

// a type for the data passed with drag event
type DragControl = {
  line: Polyline;
  handles: Circle[];
};

const addHandles = (
  group: Container,
  curve: PiecewiseCubicBezier,
  onDragMove: (
    e: Event,
    c: DragControl,
    controlIndex: number,
    pointIndex: number
  ) => void,
  onSetCurve: (_: PiecewiseCubicBezier) => void
) => {
  const parentStrokeWidth = group.parent()?.attr("stroke-width");
  const controlRadius = (parentStrokeWidth / 3) * 8;

  const color = "#d32f2f";
  group.stroke(color);

  curve.forEach((ctl: Point[], ctlIndex: number) => {
    const control: DragControl = {
      line: group.polyline(ctl as PointArray),
      handles: ctl.map((point, pointIndex) =>
        group
          .circle(2 * controlRadius)
          .center(...point)
          .fill(color)
          .attr({ cursor: "move" })
          .draggable()
          .on("dragmove", (e) => onDragMove(e, control, ctlIndex, pointIndex))
          .on("dragend", (_) => onSetCurve(curve))
          // right click remove point
          .on("contextmenu", (e) => {
            e.preventDefault();
            removePoint(curve, ctlIndex);
            onSetCurve(curve);
          })
      ),
    };
  });
};

const onDragMoveFactory = (
  curve: PiecewiseCubicBezier,
  svgCurve: Path,
  svgDerivative: Path[] | undefined,
  magnetism: boolean,
  magnetRadius: number,
  minorUnits: number,
  continuity: number,
  uniqImageWarning: HTMLElement | null
) => {
  const magnetRadiusSquared = sqr(magnetRadius);

  return (
    e: Event,
    control: DragControl,
    ctlIndex: number,
    pointIndex: number
  ) => {
    //@ts-ignore
    const { handler, box, event } = e.detail;
    e.preventDefault();
    let p = [box.cx, box.cy];

    // ctrl key change continuity
    if ((event as MouseEvent).ctrlKey)
      continuity = 1 - +(event as MouseEvent).shiftKey;

    // magnet effect
    if (magnetism) {
      const pGrid = p.map((t) => Math.round(minorUnits * t) / minorUnits);
      if (dist2(p, pGrid) < magnetRadiusSquared) p = pGrid;
    }

    const handle = handler.el;
    // compute increment
    const delta: Point = [p[0] - handle.cx(), p[1] - handle.cy()];
    // update raw curve (array)
    moveControlPoint(curve, ctlIndex, pointIndex, delta, continuity);
    // update tangent
    control.line.plot(curve[ctlIndex]);
    // and handles
    control.handles.forEach((c, i) => c.center(...curve[ctlIndex][i]));
    // and svg curve
    svgCurve.plot(curveToString(curve));
    // and derivative
    if (svgDerivative != null) {
      const npts = svgDerivative[0].array().length;
      const pcb = derivative(curve, npts, true) as PiecewiseCubicBezier[];
      svgDerivative.forEach((v, k) => v.plot(curveToString(pcb[k])));
    }
    // and non uniq image warning
    if (uniqImageWarning != null)
      uniqImageWarning.setAttribute(
        "data-visible",
        (!uniqImage(curve)).toString()
      );
  };
};

const Canvas = ({
  xlim,
  ylim,
  minorUnits,
  showAxis,
  showGrid,
  showHandles,
  showDerivative,
  improveTouch,
  continuity,
  curve,
  onSetCurve,
  svgRef,
  uniqImageRef,
  ...props
}: CanvasProps) => {
  const elemRef = useRef<HTMLDivElement>(null);
  // we use memo here since a ref will be reset at each re-render
  const svg = useMemo(() => SVG(), []);
  svgRef.current = svg;

  useEffect(() => {
    if (!elemRef?.current?.hasChildNodes() && elemRef.current != null) {
      svg.addTo(elemRef.current as HTMLDivElement);
    }
    svg.clear();

    const width = xlim[1] - xlim[0];
    const height = ylim[1] - ylim[0];
    const unit = width / 16;
    const factor = improveTouch ? 2 : 1;
    const strokeWidth = 0.03 * unit * factor;
    const fontSize = 0.25 * unit;
    const magnetRadius = 0.1 * unit;
    const samplesDerivative = 15;

    // curveToSet is used by events' callbacks since curve is read-only
    // see drag/add/remove
    const curveToSet: typeof curve = JSON.parse(JSON.stringify(curve));

    // force width and height otherwise later raster export fails on FF
    // we use screen resolution to get svg export at consistent size
    svg.size(
      Math.min((window.screen.height * width) / height, window.screen.width),
      Math.min((window.screen.width * height) / width, window.screen.height)
    );
    svg.viewbox(0, 0, width, height);
    svg.attr({ preserveAspectRatio: "xMidYMid meet" });

    const maingroup = svg.group();
    maingroup.attr({
      fill: "none",
      "stroke-linecap": "round",
      "stroke-linejoin": "round",
      stroke: "currentcolor",
      transform: `matrix(1, 0, 0, -1, ${-xlim[0]}, ${ylim[1]})`,
      "stroke-width": strokeWidth,
      "font-size": fontSize,
    });

    if (showGrid) addGrid(maingroup.group(), xlim, ylim, minorUnits);
    if (showAxis) addAxis(maingroup.group(), xlim, ylim);
    const svgCurve = addCurve(maingroup.group(), curveToSet, onSetCurve);
    let svgDerivative: Path[] | undefined = undefined;
    if (showDerivative)
      svgDerivative = addDerivative(
        maingroup.group(),
        curveToSet,
        samplesDerivative
      );
    if (showHandles) {
      const onDragMove = onDragMoveFactory(
        curveToSet,
        svgCurve,
        svgDerivative,
        showGrid,
        magnetRadius,
        minorUnits,
        continuity,
        uniqImageRef.current
      );
      addHandles(maingroup.group(), curveToSet, onDragMove, onSetCurve);
    }
  }, [
    xlim,
    ylim,
    minorUnits,
    showAxis,
    showGrid,
    showHandles,
    showDerivative,
    improveTouch,
    continuity,
    curve,
    elemRef,
    uniqImageRef,
    svg,
  ]);

  const { t, i18n } = useTranslation();
  return (
    <div {...props}>
      <div ref={elemRef} />
    </div>
  );
};

export { Canvas };
