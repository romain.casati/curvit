import { MutableRefObject, useState, useEffect } from "react";
import { useTranslation } from "react-i18next";

import { useTheme } from "@mui/material";
import Container from "@mui/material/Container";
import Box, { BoxProps } from "@mui/material/Box";
import TuneIcon from "@mui/icons-material/Tune";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Snackbar from "@mui/material/Snackbar";
import Slide, { SlideProps } from "@mui/material/Slide";
import Alert from "@mui/material/Alert";

// icons
import SvgIcon from "@mui/material/SvgIcon";
import UndoIcon from "@mui/icons-material/Undo";
import RedoIcon from "@mui/icons-material/Redo";
import MenuIcon from "@mui/icons-material/Menu";
import WarningAmberIcon from "@mui/icons-material/WarningAmber";

import { MenuOnOff } from "./menu-on-off";
import { MainMenu } from "./main-menu";

import { ColorMode, ExportFormat } from "../state/state-manager";
import Logo from "../logo/logo-light.svg";
import { PiecewiseCubicBezier } from "../curves/piecewise-cubic-bezier";
import { uniqImage } from "../curves/piecewise-cubic-bezier";

interface MainToolbarProps extends BoxProps {
  curve: PiecewiseCubicBezier;
  xlim: [number, number];
  ylim: [number, number];
  minorUnits: number;
  showAxis: boolean;
  showGrid: boolean;
  showHandles: boolean;
  showDerivative: boolean;
  improveTouch: boolean;
  continuity: number;
  exportFormat: ExportFormat;
  colorMode: ColorMode;
  isFullscreen: boolean;
  uniqImageRef: MutableRefObject<HTMLButtonElement | null>;
  onSetXlim: (_: [number, number]) => void;
  onSetYlim: (_: [number, number]) => void;
  onSetMinorUnits: (_: number) => void;
  onChangeShowAxis: (_: boolean) => void;
  onChangeShowGrid: (_: boolean) => void;
  onChangeShowHandles: (_: boolean) => void;
  onChangeShowDerivative: (_: boolean) => void;
  onChangeImproveTouch: (_: boolean) => void;
  onSetContinuity: (_: number) => void;
  onChangeExportFormat: (_: ExportFormat) => void;
  onReset: () => void;
  onExport: () => void;
  onImport: (_: File) => void;
  onSetColorMode: (_: ColorMode) => void;
  onPrevHistory: () => void;
  onNextHistory: () => void;
  onToggleFullscreen: () => void;
}

const slideTransition = (props: SlideProps) => (
  <Slide {...props} direction="down" />
);

const MainToolbar = ({
  curve,
  xlim,
  ylim,
  minorUnits,
  showAxis,
  showGrid,
  showHandles,
  showDerivative,
  improveTouch,
  continuity,
  exportFormat,
  colorMode,
  isFullscreen,
  uniqImageRef,
  onSetXlim,
  onSetYlim,
  onSetMinorUnits,
  onChangeShowAxis,
  onChangeShowGrid,
  onChangeShowHandles,
  onChangeShowDerivative,
  onChangeImproveTouch,
  onSetContinuity,
  onChangeExportFormat,
  onReset,
  onExport,
  onImport,
  onSetColorMode,
  onPrevHistory,
  onNextHistory,
  onToggleFullscreen,
  ...props
}: MainToolbarProps) => {
  const { t, i18n } = useTranslation();
  const [anchorOnOffMenu, setAnchorOnOffMenu] = useState<null | HTMLElement>(
    null
  );
  const [anchorMainMenu, setAnchorMainMenu] = useState<null | HTMLElement>(
    null
  );
  const [openUniqImageSnack, setOpenUniqImageSnack] = useState<boolean>(false);

  useEffect(() => {
    setOpenUniqImageSnack(!uniqImage(curve));
  }, [curve]);

  useEffect(() => {
    // undo/redo keyboard shortcuts
    const undoRedo = (e: KeyboardEvent) => {
      if (e.ctrlKey) {
        if (e.key === "z") onPrevHistory();
        else if (e.key === "y") onNextHistory();
      }
    };
    window.addEventListener("keydown", undoRedo);
    return () => window.removeEventListener("keydown", undoRedo);
  }, []);

  const theme = useTheme();

  return (
    <Box {...props}>
      <AppBar position="static">
        <Toolbar>
          <Box sx={{ flexGrow: 0 }}>
            <IconButton
              href="https://curvit.fr"
              title="curv'it!"
              aria-label="curv'it"
            >
              <SvgIcon
                component={Logo}
                viewBox="0.1 0.6 6.55 2.55"
                style={{
                  width: "unset",
                  height: "60px",
                  maxWidth: "calc(50vw - 20px - 72px",
                }}
              />
            </IconButton>
          </Box>
          <Container />
          <Box
            display="flex"
            flexDirection="row"
            sx={{
              position: "absolute",
              left: "calc(50% + 6px)",
              transform: "translateX(-50%)",
            }}
          >
            <IconButton
              edge="start"
              color="inherit"
              sx={{ mr: 2 }}
              title={t("Undo")}
              aria-label={t("Undo")}
              onClick={() => onPrevHistory()}
            >
              <UndoIcon />
            </IconButton>
            <IconButton
              edge="start"
              color="inherit"
              sx={{ mr: 2 }}
              title={t("Parameters")}
              aria-label={t("Parameters")}
              onClick={(e) => setAnchorOnOffMenu(e.currentTarget)}
            >
              <TuneIcon />
            </IconButton>
            <IconButton
              edge="start"
              color="inherit"
              title={t("Redo")}
              aria-label={t("Redo")}
              onClick={() => onNextHistory()}
            >
              <RedoIcon />
            </IconButton>
          </Box>
          <Container />
          <IconButton
            edge="start"
            title={t("multiple-images")}
            sx={{ mr: 2, color: theme.palette.warning.light }}
            aria-label={t("multiple-images")}
            data-visible={!uniqImage(curve)}
            ref={uniqImageRef}
            onClick={() => setOpenUniqImageSnack(true)}
          >
            <WarningAmberIcon />
          </IconButton>
          <IconButton
            edge="start"
            color="inherit"
            title={t("Menu")}
            onClick={(e) => setAnchorMainMenu(e.currentTarget)}
            aria-label={t("Menu")}
          >
            <MenuIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
      <MainMenu
        anchorEl={anchorMainMenu}
        onClose={() => setAnchorMainMenu(null)}
        exportFormat={exportFormat}
        colorMode={colorMode}
        isFullscreen={isFullscreen}
        onChangeExportFormat={onChangeExportFormat}
        onReset={onReset}
        onExport={onExport}
        onImport={onImport}
        onSetColorMode={onSetColorMode}
        onToggleFullscreen={onToggleFullscreen}
      />
      <MenuOnOff
        anchorEl={anchorOnOffMenu}
        onClose={() => setAnchorOnOffMenu(null)}
        xlim={xlim}
        ylim={ylim}
        minorUnits={minorUnits}
        showAxis={showAxis}
        showGrid={showGrid}
        showHandles={showHandles}
        showDerivative={showDerivative}
        improveTouch={improveTouch}
        continuity={continuity}
        onSetXlim={onSetXlim}
        onSetYlim={onSetYlim}
        onSetMinorUnits={onSetMinorUnits}
        onChangeShowAxis={onChangeShowAxis}
        onChangeShowGrid={onChangeShowGrid}
        onChangeShowHandles={onChangeShowHandles}
        onChangeShowDerivative={onChangeShowDerivative}
        onChangeImproveTouch={onChangeImproveTouch}
        onSetContinuity={onSetContinuity}
      />
      <Snackbar
        open={openUniqImageSnack}
        onClose={() => setOpenUniqImageSnack(false)}
        TransitionComponent={slideTransition}
        anchorOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
        key={"uniq-image-warning"}
        autoHideDuration={5000}
      >
        <Alert
          onClose={() => setOpenUniqImageSnack(false)}
          severity="warning"
          sx={{ width: "100%" }}
        >
          {t("multiple-images")}
        </Alert>
      </Snackbar>
    </Box>
  );
};

export { MainToolbar };
