import { useTranslation } from "react-i18next";

import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import Box, { BoxProps } from "@mui/material/Box";
import TextField from "@mui/material/TextField";

interface DialogWindowSettingsProps extends BoxProps {
  open: boolean;
  onClose: () => void;
  xlim: [number, number];
  ylim: [number, number];
  minorUnits: number;
  onSetXlim: (_: [number, number]) => void;
  onSetYlim: (_: [number, number]) => void;
  onSetMinorUnits: (_: number) => void;
}

const DialogWindowSettings = ({
  open,
  onClose,
  xlim,
  ylim,
  minorUnits,
  onSetXlim,
  onSetYlim,
  onSetMinorUnits,
  ...props
}: DialogWindowSettingsProps) => {
  const { t, i18n } = useTranslation();
  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>{t("Axis settings")}</DialogTitle>
      <DialogContent>
        <Box
          display="flex"
          flexDirection="column"
          gap="1em"
          {...props}
          marginTop=".5em"
        >
          <Box display="flex" gap="1em">
            <TextField
              label="X min"
              type="number"
              size="small"
              InputProps={{
                inputProps: {
                  min: -99,
                  max: 99,
                },
              }}
              InputLabelProps={{
                shrink: true,
              }}
              value={xlim[0]}
              onChange={(e) => onSetXlim([+e.target.value, xlim[1]])}
            />
            <TextField
              label="X max"
              type="number"
              size="small"
              InputProps={{
                inputProps: {
                  min: -99,
                  max: 99,
                },
              }}
              InputLabelProps={{
                shrink: true,
              }}
              value={xlim[1]}
              onChange={(e) => onSetXlim([xlim[0], +e.target.value])}
            />
          </Box>
          <Box display="flex" gap="1em">
            <TextField
              label="Y min"
              type="number"
              size="small"
              InputProps={{
                inputProps: {
                  min: -99,
                  max: 99,
                },
              }}
              InputLabelProps={{
                shrink: true,
              }}
              value={ylim[0]}
              onChange={(e) => onSetYlim([+e.target.value, ylim[1]])}
            />
            <TextField
              label="Y max"
              type="number"
              size="small"
              InputProps={{
                inputProps: {
                  min: -99,
                  max: 99,
                },
              }}
              InputLabelProps={{
                shrink: true,
              }}
              value={ylim[1]}
              onChange={(e) => onSetYlim([ylim[0], +e.target.value])}
            />
          </Box>
          <TextField
            label={t("Minor units")}
            type="number"
            size="small"
            InputProps={{
              inputProps: {
                min: 1,
                max: 99,
              },
            }}
            InputLabelProps={{
              shrink: true,
            }}
            value={minorUnits}
            onChange={(e) => onSetMinorUnits(+e.target.value)}
          />
        </Box>
      </DialogContent>
    </Dialog>
  );
};

export { DialogWindowSettings };
