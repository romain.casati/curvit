import { useState } from "react";
import { useTranslation } from "react-i18next";

import Box, { BoxProps } from "@mui/material/Box";
import Container from "@mui/material/Container";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import FormControlLabel from "@mui/material/FormControlLabel";
import Switch from "@mui/material/Switch";

// icons
import SettingsIcon from "@mui/icons-material/Settings";

import { DialogWindowSettings } from "./dialog-window";

interface MenuOnOffProps extends BoxProps {
  anchorEl: null | HTMLElement;
  onClose: () => void;
  xlim: [number, number];
  ylim: [number, number];
  minorUnits: number;
  showAxis: boolean;
  showGrid: boolean;
  showHandles: boolean;
  showDerivative: boolean;
  improveTouch: boolean;
  continuity: number;
  onSetXlim: (_: [number, number]) => void;
  onSetYlim: (_: [number, number]) => void;
  onSetMinorUnits: (_: number) => void;
  onChangeShowAxis: (_: boolean) => void;
  onChangeShowGrid: (_: boolean) => void;
  onChangeShowHandles: (_: boolean) => void;
  onChangeShowDerivative: (_: boolean) => void;
  onChangeImproveTouch: (_: boolean) => void;
  onSetContinuity: (_: number) => void;
}

const MenuOnOff = ({
  anchorEl,
  onClose,
  xlim,
  ylim,
  minorUnits,
  showAxis,
  showGrid,
  showHandles,
  showDerivative,
  improveTouch,
  continuity,
  onSetXlim,
  onSetYlim,
  onSetMinorUnits,
  onChangeShowAxis,
  onChangeShowGrid,
  onChangeShowHandles,
  onChangeShowDerivative,
  onChangeImproveTouch,
  onSetContinuity,
  ...props
}: MenuOnOffProps) => {
  const [openWindowSettings, setOpenWindowSettings] = useState(false);
  const open = Boolean(anchorEl);
  const { t, i18n } = useTranslation();
  return (
    <>
      <Box {...props}>
        <Menu
          anchorEl={anchorEl}
          open={open}
          onClose={onClose}
          anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
          transformOrigin={{ vertical: "top", horizontal: "center" }}
        >
          <MenuItem>
            <FormControlLabel
              control={
                <Switch
                  checked={improveTouch}
                  onChange={(e) => onChangeImproveTouch(e.target.checked)}
                />
              }
              label={t("Improve touch")}
              labelPlacement="end"
            />
          </MenuItem>
          <MenuItem>
            <FormControlLabel
              control={
                <Switch
                  checked={showAxis}
                  onChange={(e) => onChangeShowAxis(e.target.checked)}
                />
              }
              label={t("Axis")}
              labelPlacement="end"
            />
            <Container />
            <ListItemIcon
              title={t("Axis settings")}
              onClick={() => setOpenWindowSettings(true)}
            >
              <SettingsIcon />
            </ListItemIcon>
          </MenuItem>
          <MenuItem>
            <FormControlLabel
              control={
                <Switch
                  checked={showGrid}
                  onChange={(e) => onChangeShowGrid(e.target.checked)}
                />
              }
              label={t("Grid")}
              labelPlacement="end"
            />
          </MenuItem>
          <MenuItem>
            <FormControlLabel
              control={
                <Switch
                  checked={showHandles}
                  onChange={(e) => onChangeShowHandles(e.target.checked)}
                />
              }
              label={t("Handles")}
              labelPlacement="end"
            />
          </MenuItem>
          <MenuItem>
            <FormControlLabel
              control={
                <Switch
                  checked={showDerivative}
                  onChange={(e) => onChangeShowDerivative(e.target.checked)}
                />
              }
              label={t("Derivative")}
              labelPlacement="end"
            />
          </MenuItem>
          <MenuItem>
            <FormControlLabel
              control={
                <Switch
                  checked={continuity >= 1}
                  onChange={(e) => onSetContinuity(2 * +e.target.checked)}
                />
              }
              label={t("Tangent continuity")}
              title={t("info g1")}
              labelPlacement="end"
            />
          </MenuItem>
        </Menu>
      </Box>
      <DialogWindowSettings
        open={openWindowSettings}
        onClose={() => setOpenWindowSettings(false)}
        xlim={xlim}
        ylim={ylim}
        minorUnits={minorUnits}
        onSetXlim={onSetXlim}
        onSetYlim={onSetYlim}
        onSetMinorUnits={onSetMinorUnits}
      />
    </>
  );
};

export { MenuOnOff };
