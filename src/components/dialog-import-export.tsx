import { useTranslation } from "react-i18next";

import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import Box, { BoxProps } from "@mui/material/Box";
import Button from "@mui/material/Button";
import FormControl from "@mui/material/FormControl";
import NativeSelect from "@mui/material/NativeSelect";

import { exportFormats, ExportFormat, formatToLabel } from "../export/export";
import styles from "./styles.module.scss";

interface DialogImportExportProps extends BoxProps {
  open: boolean;
  onClose: () => void;
  exportFormat: ExportFormat;
  onChangeExportFormat: (_: ExportFormat) => void;
  onExport: () => void;
  onImport: (_: File) => void;
}

const DialogImportExport = ({
  open,
  onClose,
  exportFormat,
  onChangeExportFormat,
  onExport,
  onImport,
  ...props
}: DialogImportExportProps) => {
  const { t, i18n } = useTranslation();
  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>{t("Import / Export")}</DialogTitle>
      <DialogContent>
        <Box
          display="flex"
          flexDirection="column"
          gap="1em"
          justifyContent="space-evenly"
          {...props}
        >
          <Box textAlign="center">
            <Button variant="outlined" onClick={() => onExport()}>
              {t("Export")}
            </Button>
            {" " + t("to format") + " "}
            <FormControl>
              <NativeSelect
                defaultValue={exportFormat}
                onChange={(e) =>
                  onChangeExportFormat(e.target.value as ExportFormat)
                }
                inputProps={{
                  name: "format",
                  id: "input-format",
                }}
              >
                {exportFormats.map((value, index) => (
                  <option value={value} key={index}>
                    {formatToLabel(value)}
                  </option>
                ))}
              </NativeSelect>
            </FormControl>
          </Box>
          <Button variant="outlined" className={styles["import"]}>
            <label>
              <input
                type="file"
                onChange={(e) => {
                  const files = e.target?.files;
                  if (files != null && files.length) onImport(files[0]);
                }}
              />
              {t("Load curvit file")}
            </label>
          </Button>
        </Box>
      </DialogContent>
    </Dialog>
  );
};

export { DialogImportExport };
