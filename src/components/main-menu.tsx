import { useState } from "react";
import { useTranslation } from "react-i18next";

// components
import Paper, { PaperProps } from "@mui/material/Paper";
import Menu from "@mui/material/Menu";
import MenuList from "@mui/material/MenuList";
import MenuItem from "@mui/material/MenuItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemIcon from "@mui/material/ListItemIcon";
import Divider from "@mui/material/Divider";

// icons
import ImportExportIcon from "@mui/icons-material/ImportExport";
import InfoIcon from "@mui/icons-material/Info";
import KeyboardTabIcon from "@mui/icons-material/KeyboardTab";
import DarkModeIcon from "@mui/icons-material/DarkMode";
import LightModeIcon from "@mui/icons-material/LightMode";
import FullscreenIcon from "@mui/icons-material/Fullscreen";
import FullscreenExitIcon from "@mui/icons-material/FullscreenExit";

import { DialogImportExport } from "./dialog-import-export";
import { ExportFormat, ColorMode } from "../state/state-manager";

interface MainMenuProps extends PaperProps {
  anchorEl: null | HTMLElement;
  onClose: () => void;
  exportFormat: ExportFormat;
  colorMode: ColorMode;
  isFullscreen: boolean;
  onChangeExportFormat: (_: ExportFormat) => void;
  onReset: () => void;
  onExport: () => void;
  onImport: (_: File) => void;
  onSetColorMode: (_: ColorMode) => void;
  onToggleFullscreen: () => void;
}

const MainMenu = ({
  anchorEl,
  onClose,
  exportFormat,
  colorMode,
  isFullscreen,
  onChangeExportFormat,
  onReset,
  onExport,
  onImport,
  onSetColorMode,
  onToggleFullscreen,
  ...props
}: MainMenuProps) => {
  const { t, i18n } = useTranslation();
  const open = Boolean(anchorEl);
  const [openImportExport, setOpenImportExport] = useState(false);

  return (
    <>
      <Paper {...props}>
        <Menu anchorEl={anchorEl} open={open} onClose={onClose}>
          <MenuList>
            <MenuItem onClick={() => onReset()}>
              <ListItemIcon>
                <KeyboardTabIcon transform="scale(-1, 1)" />
              </ListItemIcon>
              <ListItemText>{t("Reset")}</ListItemText>
            </MenuItem>
            <MenuItem onClick={() => setOpenImportExport(true)}>
              <ListItemIcon>
                <ImportExportIcon />
              </ListItemIcon>
              <ListItemText>{t("Import / Export")}</ListItemText>
            </MenuItem>
            <MenuItem
              onClick={() =>
                onSetColorMode(colorMode === "dark" ? "light" : "dark")
              }
            >
              <ListItemIcon>
                {colorMode === "dark" ? <LightModeIcon /> : <DarkModeIcon />}
              </ListItemIcon>
              <ListItemText>{t("Dark / Light")}</ListItemText>
            </MenuItem>
            <MenuItem onClick={() => onToggleFullscreen()}>
              <ListItemIcon>
                {isFullscreen ? <FullscreenExitIcon /> : <FullscreenIcon />}
              </ListItemIcon>
              <ListItemText>{t("Fullscreen")}</ListItemText>
            </MenuItem>
            <Divider />
            <MenuItem
              component="a"
              href="https://forge.apps.education.fr/romain.casati/curvit"
              target="_blank"
              rel="noopener noreferrer"
            >
              <ListItemIcon>
                <InfoIcon />
              </ListItemIcon>
              <ListItemText>{t("About")}</ListItemText>
            </MenuItem>
          </MenuList>
        </Menu>
      </Paper>
      <DialogImportExport
        open={openImportExport}
        onClose={() => setOpenImportExport(false)}
        exportFormat={exportFormat}
        onChangeExportFormat={onChangeExportFormat}
        onImport={onImport}
        onExport={onExport}
      />
    </>
  );
};

export { MainMenu };
