import { useEffect, useRef, useMemo, useState } from "react";
import CircularProgress from "@mui/material/CircularProgress";
import { Container } from "@svgdotjs/svg.js";
import Box from "@mui/material/Box";
import { ThemeProvider, createTheme } from "@mui/material";
import CssBaseline from "@mui/material/CssBaseline";
import useMediaQuery from "@mui/material/useMediaQuery";

// mui fonts
import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";

import { stateManager, State } from "../state/state-manager";
import { History } from "../state/history";
import { Canvas } from "./canvas";
import { MainToolbar } from "./main-toolbar";
import { exportAndDownloadCurve } from "../export/export";
import styles from "./styles.module.scss";

const App = () => {
  // history
  const history = useMemo(() => new History<State>(), []);
  // state
  const [state, dispatch] = stateManager.useReducer(history);
  // fullscreen
  const [isFullscreen, setIsFullscreen] = useState(
    document.fullscreenElement != null
  );

  // manage color mode state outside of statemanager
  const systemMode = useMediaQuery("(prefers-color-scheme: dark)")
    ? "dark"
    : "light";
  const mode = stateManager.colorMode(state) ?? systemMode;
  const theme = useMemo(() => createTheme({ palette: { mode } }), [mode]);

  // keep a ref on svg in order to later export it
  const svg = useRef<Container | null>(null);

  // keep a ref on the uniq image warning
  const uniqImageRef = useRef<HTMLButtonElement | null>(null);

  // init state manager and apply body style from css module
  useEffect(() => {
    stateManager.init(dispatch);
    document.body.classList.add(styles.body);
    return () => {
      document.body.classList.remove(styles.body);
    };
  }, []);

  // build component
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      {stateManager.ready(state) ? (
        <>
          <MainToolbar
            className={styles["main-toolbar"]}
            curve={stateManager.curve(state)}
            xlim={stateManager.xlim(state)}
            ylim={stateManager.ylim(state)}
            minorUnits={stateManager.minorUnits(state)}
            showAxis={stateManager.showAxis(state)}
            showGrid={stateManager.showGrid(state)}
            showHandles={stateManager.showHandles(state)}
            showDerivative={stateManager.showDerivative(state)}
            improveTouch={stateManager.improveTouch(state)}
            continuity={stateManager.continuity(state)}
            exportFormat={stateManager.exportFormat(state)}
            colorMode={mode}
            isFullscreen={isFullscreen}
            uniqImageRef={uniqImageRef}
            onSetXlim={(xlim) =>
              dispatch({
                type: "set-xlim",
                payload: xlim,
              })
            }
            onSetYlim={(ylim) =>
              dispatch({
                type: "set-ylim",
                payload: ylim,
              })
            }
            onSetMinorUnits={(minorUnits) =>
              dispatch({
                type: "set-minor-units",
                payload: minorUnits,
              })
            }
            onChangeShowAxis={(v) =>
              dispatch({
                type: "toggle-show-axis",
                payload: v,
              })
            }
            onChangeShowGrid={(v) =>
              dispatch({
                type: "toggle-show-grid",
                payload: v,
              })
            }
            onChangeShowHandles={(v) =>
              dispatch({
                type: "toggle-show-handles",
                payload: v,
              })
            }
            onChangeShowDerivative={(v) =>
              dispatch({
                type: "toggle-show-derivative",
                payload: v,
              })
            }
            onChangeImproveTouch={(v) =>
              dispatch({ type: "toggle-improve-touch", payload: v })
            }
            onSetContinuity={(v) =>
              dispatch({ type: "set-continuity", payload: v })
            }
            onChangeExportFormat={(v) =>
              dispatch({ type: "set-export-format", payload: v })
            }
            onReset={() => stateManager.reset(dispatch)}
            onExport={() => {
              const svgString = svg.current?.svg() ?? "";
              exportAndDownloadCurve(state, svgString, "graph");
            }}
            onImport={async (file: File) => {
              const reader = new FileReader();
              reader.readAsText(file, "UTF-8");
              const content = await new Promise<string>((resolve) => {
                reader.onload = (e) => resolve(e.target?.result as string);
              });
              stateManager.load(JSON.parse(content), dispatch);
            }}
            onSetColorMode={(v) =>
              dispatch({ type: "set-color-mode", payload: v })
            }
            onPrevHistory={() => dispatch({ type: "previous-state" })}
            onNextHistory={() => dispatch({ type: "next-state" })}
            onToggleFullscreen={async () => {
              const isFs = document.fullscreenElement != null;
              const promise = isFs
                ? document.exitFullscreen()
                : document.body.requestFullscreen();
              await promise;
              setIsFullscreen(document.fullscreenElement != null);
            }}
          />
          <Canvas
            className={styles["canvas"]}
            xlim={stateManager.xlim(state)}
            ylim={stateManager.ylim(state)}
            minorUnits={stateManager.minorUnits(state)}
            showAxis={stateManager.showAxis(state)}
            showGrid={stateManager.showGrid(state)}
            showHandles={stateManager.showHandles(state)}
            showDerivative={stateManager.showDerivative(state)}
            improveTouch={stateManager.improveTouch(state)}
            continuity={stateManager.continuity(state)}
            curve={stateManager.curve(state)}
            onSetCurve={(curve) =>
              dispatch({ type: "set-curve", payload: curve })
            }
            svgRef={svg}
            uniqImageRef={uniqImageRef}
          />
        </>
      ) : (
        <Box
          display="flex"
          justifyContent="center"
          alignItems="center"
          width="100vw"
          height="100vh"
        >
          <CircularProgress />
        </Box>
      )}
    </ThemeProvider>
  );
};

export default App;
