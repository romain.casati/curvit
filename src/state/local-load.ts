const copy = (value: any): any => JSON.parse(JSON.stringify(value));

type ToKeep = { [key: string]: boolean };

const filterProps = (value: any, toKeep?: ToKeep) => {
  const res = copy(value);
  if (toKeep != null) for (const v in res) if (!toKeep[v]) delete res[v];
  return res;
};

const localLoad = async (
  key: string,
  defaultValue: any,
  toKeep?: ToKeep
): Promise<any> => {
  // prepare result
  const res = copy(defaultValue);
  // get local backup
  let local: any = {};
  try {
    const tmp = window.localStorage.getItem(key);
    if (tmp != null) local = JSON.parse(tmp);
  } catch (e) {}
  // read result properties from backup
  Object.assign(res, filterProps(local, toKeep));
  return res;
};

const localSave = async (key: string, value: any, toKeep?: ToKeep) => {
  try {
    value = filterProps(value, toKeep);
    window.localStorage.setItem(key, JSON.stringify(value));
  } catch (e) {}
};

export { localLoad, localSave, filterProps, copy };
