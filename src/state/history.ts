class History<Type> {
  _passed: Type[] = [];
  _future: Type[] = [];

  constructor() {}

  push(v: Type) {
    if (v == this._passed.slice(-1)[0]) return;
    this._passed.push(v);
    this._future.length = 0; // length property is read/write
  }

  prev(current: Type): undefined | Type {
    const res = this._passed.pop();
    if (res != null) this._future.push(current);
    return res;
  }

  next(current: Type): undefined | Type {
    const res = this._future.pop();
    if (res != null) this._passed.push(current);
    return res;
  }
}

export { History };
