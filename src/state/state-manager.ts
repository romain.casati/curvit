import { type Dispatch } from "react";
/* we use immer to fake mutable state */
import { useImmerReducer } from "use-immer";
import { localLoad, localSave, filterProps, copy } from "./local-load";
import { ExportFormat } from "../export/export";
import { PiecewiseCubicBezier } from "../curves/piecewise-cubic-bezier";
import { History } from "./history";

type ColorMode = "light" | "dark" | undefined;

/* our global state type */
interface State {
  ready: boolean;
  xlim: [number, number];
  ylim: [number, number];
  minorUnits: number;
  showAxis: boolean;
  showGrid: boolean;
  showHandles: boolean;
  showDerivative: boolean;
  improveTouch: boolean;
  continuity: number;
  curve: [number, number][][];
  exportFormat: ExportFormat;
  colorMode: ColorMode;
}

/* our dispatched action type */
interface Action {
  type:
    | "init"
    | "previous-state"
    | "next-state"
    | "set-xlim"
    | "set-ylim"
    | "set-minor-units"
    | "toggle-show-axis"
    | "toggle-show-grid"
    | "toggle-show-handles"
    | "toggle-show-derivative"
    | "toggle-improve-touch"
    | "set-continuity"
    | "set-curve"
    | "set-export-format"
    | "set-color-mode";
  payload?:
    | number
    | [number, number]
    | boolean
    | PiecewiseCubicBezier
    | ExportFormat
    | State
    | ColorMode;
}

/* list state items that need to be saved */
const saved: { [key in keyof State]: boolean } = {
  ready: false,
  xlim: true,
  ylim: true,
  minorUnits: true,
  showAxis: true,
  showGrid: true,
  showHandles: true,
  showDerivative: true,
  improveTouch: true,
  continuity: true,
  exportFormat: true,
  colorMode: true,
  curve: true,
};

/* our initial state (if nothing is loaded from local backup) */
const initialState: State = {
  ready: false,
  xlim: [-8, 8],
  ylim: [-4, 4],
  minorUnits: 2,
  showAxis: true,
  showGrid: true,
  showHandles: true,
  showDerivative: false,
  improveTouch: false,
  continuity: 2,
  exportFormat: "svg",
  colorMode: undefined,
  curve: [
    [
      [-7, 0],
      [-4, 3],
    ],
    [
      [-2.5, -2],
      [0, -2],
      [2.5, -2],
    ],
    [
      [2, 3],
      [4, 3],
      [6, 3],
    ],
    [
      [5.5, -2],
      [7, 0],
    ],
  ],
};

/* where our global logic takes place */
const reducer = (history: History<State>) => (state: State, action: Action) => {
  const { type, payload } = action;
  let oldState = copy(state);
  switch (type) {
    case "init":
      Object.assign(state, payload);
      state.ready = true;
      oldState = null; // do not push old state to history
      break;
    case "previous-state":
      oldState = null; // do not push old state to history
      const previousState = history.prev(copy(state));
      if (previousState != null) Object.assign(state, previousState);
      break;
    case "next-state":
      oldState = null; // do not push old state to history
      const nextState = history.next(copy(state));
      if (nextState != null) Object.assign(state, nextState);
      break;
    case "set-xlim":
      state.xlim = payload as [number, number];
      break;
    case "set-ylim":
      state.ylim = payload as [number, number];
      break;
    case "set-minor-units":
      state.minorUnits = payload as number;
      break;
    case "toggle-show-axis":
      state.showAxis = payload as boolean;
      break;
    case "toggle-show-grid":
      state.showGrid = payload as boolean;
      break;
    case "toggle-show-handles":
      state.showHandles = payload as boolean;
      break;
    case "toggle-show-derivative":
      state.showDerivative = payload as boolean;
      break;
    case "toggle-improve-touch":
      state.improveTouch = payload as boolean;
      break;
    case "set-continuity":
      state.continuity = payload as number;
      break;
    case "set-curve":
      state.curve = payload as PiecewiseCubicBezier;
      break;
    case "set-export-format":
      state.exportFormat = payload as ExportFormat;
      break;
    case "set-color-mode":
      state.colorMode = payload as ColorMode;
      break;
    default:
      throw Error(`Internal error: unrecognized action type: '${type}'!`);
  }
  localSave("state", state, saved);
  if (oldState != null && oldState != state) history.push(oldState);
};

const stateManager = {
  load: (state: State, dispatch: Dispatch<Action>) =>
    dispatch({ type: "init", payload: state }),
  init: async (dispatch: Dispatch<Action>) =>
    stateManager.load(await localLoad("state", initialState, saved), dispatch),
  reset: (dispatch: Dispatch<Action>) =>
    stateManager.load(initialState, dispatch),
  filter: (state: State): any => filterProps(state, saved),
  useReducer: (history: History<State>) =>
    useImmerReducer(reducer(history), initialState),
  ready: (state: State): boolean => state.ready,
  xlim: (state: State): [number, number] => state.xlim,
  ylim: (state: State): [number, number] => state.ylim,
  minorUnits: (state: State): number => state.minorUnits,
  showAxis: (state: State): boolean => state.showAxis,
  showGrid: (state: State): boolean => state.showGrid,
  showHandles: (state: State): boolean => state.showHandles,
  showDerivative: (state: State): boolean => state.showDerivative,
  improveTouch: (state: State): boolean => state.improveTouch,
  continuity: (state: State): number => state.continuity,
  curve: (state: State): PiecewiseCubicBezier => state.curve,
  exportFormat: (state: State): ExportFormat => state.exportFormat,
  colorMode: (state: State): ColorMode => state.colorMode,
};

export { stateManager, type State, type ExportFormat, type ColorMode };
